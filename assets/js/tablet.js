// Circle
    function circleMotion (order, page, time, delay, ease, y) {
    TweenMax.to(".circle." + order + "." + page, time, {
        ease: ease,
        delay: delay,
        y: y
    });
    }

// Appear
    function page1_element_app (element, time, delay) {
        TweenMax.to(".content.page-1 " + element, time, {
            ease: Power3.easeOut,
            delay: delay,
            y: 0,
            opacity: 1
        })
    }

    function page2_element_app (element, time, delay) {
        TweenMax.to(".content.page-2 " + element, time, {
            ease: Power3.easeOut,
            delay: delay,
            y: 0,
            opacity: 1
        })
    }

    function page2ex_element_app(alphabet, element, time, delay) {
        TweenMax.to(".content.page-2" + alphabet + " " + element, time, {
            ease: Power3.easeOut,
            delay: delay,
            opacity: 1
        })
    }

    function page4_element_app(element, time, delay) {
        TweenMax.to(".content.page-4 " + element, time, {
            ease: Power3.easeOut,
            delay: delay,
            x: 0,
            opacity: 1
        })
    }

// Diss
    function page1_element_dis (element, time, delay) {
        TweenMax.to(".content.page-1 " + element, time, {
            ease: Power3.easeIn,
            delay: delay,
            y: "-3vw",
            opacity: 0
        })
    }

    function page2_element_dis (element, time, delay) {
        TweenMax.to(".content.page-2 " + element, time, {
            ease: Power3.easeIn,
            delay: delay,
            y: "-3vw",
            opacity: 0
        })
    }

    function page2ex_element_dis(alphabet, element, time, delay) {
        TweenMax.to(".content.page-2" + alphabet + " " + element, time, {
            ease: Power3.easeIn,
            delay: delay,
            opacity: 0
        })
    }

    function page4_element_dis(element, time, delay) {
        TweenMax.to(".content.page-4 " + element, time, {
            ease: Power3.easeIn,
            delay: delay,
            opacity: 0
        })
    }

// HR
    function page1_hr (time, delay, height) {
        TweenMax.to(".content.page-1 hr", time, {
            ease: Power3.easeOut,
            delay: delay,
            height: height
        })
    }

    function page2_hr (time, delay, width, opacity) {
        TweenMax.to(".content.page-2 hr", time, {
            ease: Power3.easeOut,
            delay: delay,
            width: width,
            opacity: opacity
        })
    }

    function page2ex_hr(alphabet, time, delay, width, opacity) {
        TweenMax.to(".content.page-2" + alphabet + " hr", time, {
            ease: Power3.easeOut,
            delay: delay,
            width: width,
            opacity: opacity
        })
    }

// Background
    function backgroundScale (time, delay, scale, ease) {
        TweenMax.to(".background.first", time, {
            ease: ease,
            delay: delay,
            scale: scale
        })
    }


/*#########################################################################*/
/*                          MAIN ANIMATION                                 */
/*#########################################################################*/
// Openenr
    circleMotion("first", "page-1", 2, 0, Power3.easeOut, "40vw");
    circleMotion("second", "page-1", 2, .3, Power3.easeOut, "60vw");
    circleMotion("third", "page-1", 2, .6, Power3.easeOut, "80vw");
    page1_element_app("h1.title", 1.5, 0);
    page1_element_app("h3.infoTitle", 1.5, 1);
    page1_element_app(".login button", 1.5, 1.5);
    page1_element_app("i", 1.5, 2);
    page1_hr(1.5, .5, "80%");

/*#########################################################################*/
/*                          PAGE 2                                         */
/*#########################################################################*/
function page2_app () {
    // Page 1 Diss
        circleMotion("first", "page-1", 3, 0, Power3.easeIn, "150vh");
        circleMotion("second", "page-1", 3, .15, Power3.easeIn, "160vh");
        circleMotion("third", "page-1", 3, .3, Power3.easeIn, "170vh");
        page1_element_dis("h1.title", 1.5, 0);
        page1_element_dis("h3.infoTitle", 1.5, 0);
        page1_element_dis("i", .5, 0);
        page1_hr(1.5, .5, "0vw", 0);
        TweenMax.to(".content.page-1", 0, {
            zIndex: "4",
            delay: 2,
            y: "120vh"
        })
    // Page 2 Show
        TweenMax.to(".content.page-2", 0, {
            zIndex: "5",
            y: 0
        })
        page2_hr(1.5, 2.3, "100%", 1);
        circleMotion("third", "page-2", 3, 1, Power3.easeOut, "50vw");
        page2_element_app("h3", 1.5, 2.3);
        page2_element_app("p", 1.5, 2.3);
        page2_element_app(".mobileNavigator", .5, 2.3);
        page2_element_app(".page-pagination", 1.5, 2.3);
        backgroundScale(3, 1, 1.1, Power3.easeInOut);
        TweenMax.to(".login button", 2, {
            ease: Power3.easeInOut,
            opacity: 0,
        })
        setTimeout(() => {
            document.getElementById("firstPage").classList.add('active');            
        }, 2500);
        TweenMax.to(".page-pagination", 2, {
            opacity: 1,
            delay: 1.5
        });
}

function page2_dis () {
    // Page 2 Dis
        TweenMax.to(".content.page-2", 0, {
            zIndex: "4",
            delay: 2,
            y: "120vh"
        })
        circleMotion("third", "page-2", 3, 0.1, Power3.easeIn, "170vh");
        page2_element_dis("h3", 1.5, 0);
        page2_element_dis("p", 1.5, 0);
        page2_element_dis(".mobileNavigator", .5, 0);
        page2_element_dis(".page-pagination", 1.5, .3);
        page2_hr(1.5, .3, "0%", 0);
        backgroundScale(3, 0, 1, Power3.easeInOut);
        TweenMax.to(".login button", 2, {
            ease: Power3.easeInOut,
            opacity: 1,
            delay: 1.5
        })
        TweenMax.to(".content.page-1", 0, {
            zIndex: "5",
            y: 0
        })
        setTimeout(() => {
            document.getElementById("firstPage").classList.remove('active');            
        }, 1500);
    // Page 1 Show
        circleMotion("first", "page-1", 2, 1, Power3.easeOut, "40vw");
        circleMotion("second", "page-1", 2, 1.3, Power3.easeOut, "60vw");
        circleMotion("third", "page-1", 2, 1.6, Power3.easeOut, "80vw");
        page1_element_app("h1.title", 1.5, 2);
        page1_element_app("h3.infoTitle", 1.5, 2);
        page1_element_app(".login button", 1.5, 2.5);
        page1_element_app("i", .5, 2);
        page1_hr(1.5, .5, "80%");
        TweenMax.to(".page-pagination", 2, {
            opacity: 0,
            delay: 1.5
        });
}


/*#########################################################################*/
/*                          PAGE 2b                                        */
/*#########################################################################*/
function page2b_app() {
    // Page 2 Diss
        circleMotion("third", "page-2", 2.5, 0, Power3.easeIn, "170vw");
        page2_element_dis("h3", 1.5, 0);
        page2_element_dis("p", 1.5, 0);
        page2_element_dis(".mobileNavigator", .5, 0);
        page2_hr(1.5, .3, "0%", 0);
        page2_element_dis(".page-pagination",0,1);
        TweenMax.to(".content.page-2", 0, {
            zIndex: "4",
            delay: 2,
            y: "120vh"
        })
        TweenMax.to(".content.page-2 .mobileNavigator", 1, {
            opacity: 0
        })
        setTimeout(() => {
            document.getElementById("firstPage").classList.remove('active');            
        }, 1500);
    // Page 2b
        circleMotion("third", "page-2ex", 2.5, 1, Power3.easeOut, "50vw");
        TweenMax.to(".content.page-2b", 0, {
            zIndex: "5",
            y: 0
        })
        page2ex_element_app('b', "h3", 1.5, 1.5);
        page2ex_element_app('b', ".page-pagination", 1.5, 1.5);
        page2ex_element_app('b', ".mobileNavigator", .5, 3);
        page2ex_hr('b', 1.5, 1.5, "100%", 1);
        TweenMax.to(".right.navigator", 2, {
            ease: Power3.easeInOut,
            opacity: 0
        })
        TweenMax.to(".background.second", 3, {
            ease: Power3.easeOut,
            opacity: 1,
            delay: 1.5,
            scale: 1.1
        })
        setTimeout(() => {
            document.getElementById("secondPage").classList.add('active');            
        }, 1500);
}

function page2b_dis() {
    // Page 2b
        circleMotion("third", "page-2ex", 2.5, 0, Power3.easeIn, "170vw");
        TweenMax.to(".content.page-2b", 0, {
            zIndex: "4",
            delay: 2,
            y: "120vh"
        })
        page2ex_element_dis('b', "h3", 1.5, 0);
        page2ex_element_dis('b', ".page-pagination", 0, 1);
        page2ex_element_dis('b', ".mobileNavigator", .5, 2);
        page2ex_hr('b', 1, 0, "0%", 0);
        TweenMax.to(".right.navigator", 2, {
            ease: Power3.easeInOut,
            opacity: 1
        })
        TweenMax.to(".background.second", 3, {
            ease: Power3.easeIn,
            opacity: 0,
            delay: 0,
            scale: 1
        })
        setTimeout(() => {
            document.getElementById("secondPage").classList.remove('active');            
        }, 1500);
    // Page 2
        circleMotion("third", "page-2", 2.5, 1, Power3.easeOut, "50vw");
        page2_element_app("h3", 1.5, 2);
        page2_element_app("p", 1.5, 2);
        page2_element_app(".mobileNavigator", .5, 2);
        page2_element_app(".page-pagination",1.5,1);
        page2_hr(1.5, 2, "100%", 1);
        TweenMax.to(".content.page-2", 0, {
            zIndex: "5",
            y: 0
        })
        TweenMax.to(".content.page-2 .mobileNavigator", 1, {
            opacity: 1,
            delay: 2
        })
        setTimeout(() => {
            document.getElementById("firstPage").classList.add('active');            
        }, 1500);
}

/*#########################################################################*/
/*                          PAGE 2c                                        */
/*#########################################################################*/
function page2c_app() {
    // Page 2b
        circleMotion("third", "page-2ex", 2.5, 0, Power3.easeIn, "170vw");
        TweenMax.to(".content.page-2b", 0, {
            zIndex: "4",
            delay: 2,
            y: "120vh"
        })
        page2ex_element_dis('b', "h3", 1.5, 0);
        page2ex_element_dis('b', ".page-pagination", 0, 1);
        page2ex_element_dis('b', ".mobileNavigator", .5, 2);
        page2ex_hr('b', 1, .5, "0%", 0);
        setTimeout(() => {
            document.getElementById("secondPage").classList.remove('active');            
        }, 1500);
    // Page 2c
        circleMotion("third", "page-2", 2.5, 1, Power3.easeOut, "50vw");
        TweenMax.to(".content.page-2c", 0, {
            zIndex: "5",
            y: 0
        })
        page2ex_element_app('c', "h3", 1.5, 2);
        page2ex_element_app('c', ".page-pagination", 1.5, 2);
        page2ex_element_app('c', ".mobileNavigator", .5, 3);
        page2ex_hr('c', 1.5, 1.5, "100%", 1);
        TweenMax.to(".background.third", 3, {
            ease: Power3.easeOut,
            opacity: 1,
            delay: 1.5,
            scale: 1.1
        })
        setTimeout(() => {
            document.getElementById("thirdPage").classList.add('active');            
        }, 1500);
}

function page2c_dis() {
    // Page 2c
        circleMotion("third", "page-2", 2.5, 0, Power3.easeIn, "170vw");
        TweenMax.to(".content.page-2c", 0, {
            zIndex: "4",
            delay: 2,
            y: "120vh"
        })
        page2ex_element_dis('c', "h3", 1.5, 0);
        page2ex_element_dis('c', ".page-pagination", 0, 1);
        page2ex_element_dis('c', ".mobileNavigator", .5, 2);
        page2ex_hr('c', 1, .5, "0%", 0);
        setTimeout(() => {
            document.getElementById("thirdPage").classList.remove('active');            
        }, 1500);
    // Page 2b
        circleMotion("third", "page-2ex", 2.5, 1, Power3.easeOut, "50vw");
        TweenMax.to(".content.page-2b", 0, {
            zIndex: "5",
            y: 0
        })
        page2ex_element_app('b', "h3", 1.5, 2);
        page2ex_element_app('b', ".page-pagination", 1.5, 2);
        page2ex_element_app('b', ".mobileNavigator", .5, 3);
        page2ex_hr('b', 1.5, 1.5, "100%", 1);
        TweenMax.to(".background.third", 3, {
            ease: Power3.easeOut,
            opacity: 0,
            delay: 1.5,
            scale: 1
        })
        setTimeout(() => {
            document.getElementById("secondPage").classList.add('active');            
        }, 1500);
}

/*#########################################################################*/
/*                          PAGE 2d                                        */
/*#########################################################################*/
function page2d_app() {
    // Page 2c
        circleMotion("third", "page-2", 2.5, 0, Power3.easeIn, "170vw");
        TweenMax.to(".content.page-2c", 0, {
            zIndex: "4",
            delay: 2,
            y: "120vh"
        })
        page2ex_element_dis('c', "h3", 1.5, 0);
        page2ex_element_dis('c', ".page-pagination", 0, 1);
        page2ex_element_dis('c', ".mobileNavigator", 1.5, 2);
        page2ex_hr('c', 1, .5, "0%", 0);
        setTimeout(() => {
            document.getElementById("thirdPage").classList.remove('active');            
        }, 1500);
    // Page 2D
        circleMotion("third", "page-2ex", 2.5, 1, Power3.easeOut, "50vw");
        TweenMax.to(".content.page-2d", 0, {
            zIndex: "5",
            y: 0
        })
        page2ex_element_app('d', "h3", 1.5, 2);
        page2ex_element_app('d', ".page-pagination", 1.5, 2);
        page2ex_element_app('d', ".mobileNavigator", 1.5, 3);
        page2ex_hr('d', 1.5, 1.5, "100%", 1);
        TweenMax.to(".background.fourth", 3, {
            ease: Power3.easeOut,
            opacity: 1,
            delay: 1.5,
            scale: 1.1
        })
        setTimeout(() => {
            document.getElementById("fourthPage").classList.add('active');            
        }, 1500);
}

function page2d_dis() {
    // Page 2D
        circleMotion("third", "page-2ex", 2.5, 0, Power3.easeIn, "170vw");
        TweenMax.to(".content.page-2d", 0, {
            zIndex: "4",
            delay: 2,
            y: "120vh"
        })
        page2ex_element_dis('d', "h3", 1.5, 0);
        page2ex_element_dis('d', ".page-pagination", 0, 1);
        page2ex_element_dis('d', ".mobileNavigator", 1.5, 2);
        page2ex_hr('d', 1, .5, "0%", 0);
        setTimeout(() => {
            document.getElementById("fourthPage").classList.remove('active');            
        }, 1500);
    // Page 2C
        circleMotion("third", "page-2", 2.5, 1, Power3.easeOut, "50vw");
        TweenMax.to(".content.page-2c", 0, {
            zIndex: "5",
            y: 0
        })
        page2ex_element_app('c', "h3", 1.5, 2);
        page2ex_element_app('c', ".page-pagination", 1.5, 2);
        page2ex_element_app('c', ".mobileNavigator", 1.5, 3);
        page2ex_hr('c', 1.5, 1.5, "100%", 1);
        TweenMax.to(".background.fourth", 3, {
            ease: Power3.easeOut,
            opacity: 0,
            delay: 1.5,
            scale: 1
        })
        setTimeout(() => {
            document.getElementById("thirdPage").classList.add('active');            
        }, 1500);
}

/*#########################################################################*/
/*                          PAGE 4                                         */
/*#########################################################################*/
function page4_app () {
    // Page 1 Diss
        circleMotion("first", "page-1", 3, 0, Power3.easeIn, "150vh");
        circleMotion("second", "page-1", 3, .15, Power3.easeIn, "160vh");
        circleMotion("third", "page-1", 3, .3, Power3.easeIn, "170vh");
        page1_element_dis("h1.title", 1.5, 0);
        page1_element_dis("h3.infoTitle", 1.5, 0);
        page1_element_dis("i", 1.5, 0);
        page1_hr(1.5, .5, "0vw", 0);
        TweenMax.to(".content.page-1", 0, {
            zIndex: "4",
            delay: 1.5
        })
        TweenMax.to(".login button", 2, {
            ease: Power3.easeInOut,
            opacity: 0,
        })
    // Page 4 App
        TweenMax.to(".content.page-4", 0, {
            zIndex: "5",
            delay: 3
        })
        circleMotion("first", "page-4", 2, 2, Power3.easeOut, "40vw");
        circleMotion("second", "page-4", 2, 2.3, Power3.easeOut, "60vw");
        circleMotion("third", "page-4", 2, 2.6, Power3.easeOut, "80vw");
        page4_element_app("*", 2, 2);
        TweenMax.to(".content.page-1", 0, {
            zIndex: "4",
            delay: 1.5
        })
}

function page4_dis () {
    // Page 4 Diss
        circleMotion("first", "page-4", 3, 0, Power3.easeIn, "150vh");
        circleMotion("second", "page-4", 3, .15, Power3.easeIn, "160vh");
        circleMotion("third", "page-4", 3, .3, Power3.easeIn, "170vh");
        page4_element_dis("*", 2, 0);
        TweenMax.to(".content.page-1", 0, {
            zIndex: "5",
            delay: 2
        })
        TweenMax.to(".content.page-4", 0, {
            zIndex: "4",
            delay: 1.5
        })
    // Page 1 App
        circleMotion("first", "page-1", 2, 2, Power3.easeOut, "40vw");
        circleMotion("second", "page-1", 2, 2.3, Power3.easeOut, "60vw");
        circleMotion("third", "page-1", 2, 2.6, Power3.easeOut, "80vw");
        page1_element_app("h1.title", 1.5, 2);
        page1_element_app("h3.infoTitle", 1.5, 3);
        page1_element_app(".login button", 1.5, 3.5);
        page1_element_app("i", 1.5, 2);
        page1_hr(1.5, .5, "80%");
}


/*#########################################################################*/
/*                          PAGE 5                                         */
/*#########################################################################*/
function page5_app(type) {
    // Page 4 Diss
        TweenMax.to(".circle.third.page-4", 2, {
            y: "100vw",
            ease: Power3.easeIn,
        })
        circleMotion("first", "page-4", 2, .45, Power3.easeIn, "150vh");
        circleMotion("second", "page-4", 2, .15, Power3.easeIn, "160vh");
        circleMotion("third", "page-4", 2, 0, Power3.easeIn, "170vh");
        TweenMax.to(".content.page-4 .row", 2, {
            y: "150vh",
            ease: Power3.easeIn,
            delay: .3
        })
        TweenMax.to("#wrapper", 0, {
            zIndex: 2,
            delay: 3
        })
        TweenMax.to(".content.page-4", 0, {
            zIndex: 2,
            delay: 4
        })
    // Page 5 App
        circleMotion("first", "page-5", 2, 1.5, Power3.easeOut, "50vw");
        circleMotion("second", "page-5", 2, 1.8, Power3.easeOut, "65vw");
        circleMotion("third", "page-5", 2, 2.1, Power3.easeOut, "80vw");
        TweenMax.to(".content.page-5." + type, 2, {
            y: "0vh",
            ease: Power3.easeOut,
            delay: 2
        })
        TweenMax.to(".content.page-5." + type, 0, {
            zIndex: 5,
        })
        TweenMax.to(".background.first", 3, {
            ease: Power3.easeInOut,
            scale: 1.2,
            delay: 1
        })
}

function page5_dis (type) {
    // Page 5 Dis
        TweenMax.to(".background.first", 3, {
            ease: Power3.easeInOut,
            scale: 1,
            delay: 1
        })
        circleMotion("first", "page-5", 2, .6, Power3.easeIn, "150vh");
        circleMotion("second", "page-5", 2, .3, Power3.easeIn, "160vh");
        circleMotion("third", "page-5", 2, 0, Power3.easeIn, "170vh");
        TweenMax.to(".content.page-5." + type, 2, {
            y: "150vh",
            ease: Power3.easeIn,
        })
        TweenMax.to(".content.page-5." + type, 0, {
            zIndex: 4,
            delay: 3
        })
    // Page 4 App
        TweenMax.to(".circle.third.page-4", 2, {
            x: "80vw",
            delay: 2.4,
        })
        TweenMax.to(".content.page-4 .row", 2, {
            y: "0vh",
            ease: Power3.easeOut,
            delay: 2.7
        })
        circleMotion("first", "page-4", 2.2, 2, Power3.easeOut, "40vw");
        circleMotion("second", "page-4", 2.2, 2.4, Power3.easeOut, "60vw");
        TweenMax.to("#wrapper", 0, {
            zIndex: 5,
            delay: 2
        })
        TweenMax.to(".content.page-4", 0, {
            zIndex: 5,
            delay: 2.2
        })
}