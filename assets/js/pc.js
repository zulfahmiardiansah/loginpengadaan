// Circle
    function circleMotion(order, page, time, delay, ease, x) {
    TweenMax.to(".circle." + order + "." + page, time, {
    ease: ease,
    delay: delay,
    x: x
    });
    }

// Element 
    function page_element_app(number, element, time, delay) {
        TweenMax.to(".content.page-" + number + " " + element, time, {
            ease: Power3.easeOut,
            delay: delay,
            x: 0,
            opacity: 1
        })
    }

    function page_element_dis(number, element, time, delay) {
        TweenMax.to(".content.page-" + number + " " + element, time, {
            ease: Power3.easeIn,
            delay: delay,
            x: "-3vw",
            opacity: 0
        })
    }

    function page4_element_dis(element, time, delay) {
        TweenMax.to(".content.page-4 " + element, time, {
            ease: Power3.easeIn,
            delay: delay,
            opacity: 0
        })
    }

// HR
    function page_hr(number, time, delay, height, opacity) {
        TweenMax.to(".content.page-" + number + " hr", time, {
            ease: Power3.easeOut,
            delay: delay,
            height: height,
            opacity: opacity
        })
    }

    function page2_hr(time, delay, width, opacity) {
        TweenMax.to(".content.page-2 hr", time, {
            ease: Power3.easeOut,
            delay: delay,
            width: width,
            opacity: opacity
        })
    }

    function page2ex_hr(alphabet, time, delay, width, opacity) {
        TweenMax.to(".content.page-2" + alphabet + " hr", time, {
            ease: Power3.easeOut,
            delay: delay,
            width: width,
            opacity: opacity
        })
    }

// Background
    function backgroundScale(time, delay, scale, ease) {
        TweenMax.to(".background.first", time, {
            ease: ease,
            delay: delay,
            scale: scale
        })
    }

/*#########################################################################*/
/*                          MAIN ANIMATION                                 */
/*#########################################################################*/
    // Opener
        circleMotion("first", "page-1", 2, 0, Power3.easeOut, "0vw");
        circleMotion("second", "page-1", 2, .3, Power3.easeOut, "15vw");
        circleMotion("third", "page-1", 2, .6, Power3.easeOut, "30vw");
        page_element_app(1, "h1.title", 1.5, 0);
        page_element_app(1, "h3.infoTitle", 1.5, 0);
        page_hr(1, 1.5, .5, "80%", 1);

    // State
        var state = "page-1";
        var play = false;

    // Navigator
        function infoNavigator() {
            if (!play) {
                play = true;
                switch (state) {
                    case "page-1":
                        page2_app();
                        state = "page-2";
                        break;
                    case "page-2":
                        page2_dis();
                        state = "page-1";
                        break;
                    case "page-2b":
                        page2b_dis();
                        state = "page-2";
                        break;
                    case "page-2c":
                        page2c_dis();
                        state = "page-2b";
                        break;
                    case "page-2d":
                        page2d_dis();
                        state = "page-2c";
                        break;
                    default:
                        break;
                }
                setTimeout(() => {
                    play = false;
                }, 3000);
            }
        }

        function secondInfoNavigator() {
            if (!play) {
                play = true;
                switch (state) {
                    case "page-2":
                        page2b_app();
                        state = "page-2b";
                        break;
                    case "page-2b":
                        page2c_app();
                        state = "page-2c";
                        break;
                    case "page-2c":
                        page2d_app();
                        state = "page-2d";
                        break;
                    default:
                        break;
                }
                setTimeout(() => {
                    play = false;
                }, 3000);
            }
        }

/*#########################################################################*/
/*                          PAGE 2                                         */
/*#########################################################################*/
// Appear
    function page2_app() {
        // Page 1 Diss
            circleMotion("first", "page-1", 2, .6, Power3.easeIn, "100vw");
            circleMotion("second", "page-1", 2, .3, Power3.easeIn, "115vw");
            circleMotion("third", "page-1", 2, 0, Power3.easeIn, "130vw");
            page_element_dis(1, "h1.title", 1.5, 0);
            page_element_dis(1, "h3.infoTitle", 1.5, 0);
            page_hr(1, 1.5, .5, "0vw", 0);
        // Page 2 Show
            circleMotion("third", "page-2", 3, 1, Power3.easeOut, "0vw");
            page_element_app(2, "h3", 1.5, 2.3);
            page_element_app(2, "p", 1.5, 2.3);
            page2_hr(1.5, 2.3, "100%", 1);
            backgroundScale(3, 1, 1.1, Power3.easeInOut);
            TweenMax.to(".login button", 2, {
                ease: Power3.easeInOut,
                opacity: 0,
            })
            TweenMax.to(".left.navigator", 2, {
                ease: Power3.easeInOut,
                scaleX: -1
            })
            TweenMax.to(".right.navigator", 2, {
                ease: Power3.easeInOut,
                opacity: 1
            })
    }

// Diss
    function page2_dis() {
        // Page 2 Diss
            circleMotion("third", "page-2", 3, 0, Power3.easeIn, "130vw");
            page_element_dis(2, "h3", 1.5, 0.3);
            page_element_dis(2, "p", 1.5, 0.3);
            page2_hr(1.5, 1, "0%", 0);
            backgroundScale(3, 0, 1, Power3.easeInOut);
            TweenMax.to(".login button", 2, {
                ease: Power3.easeInOut,
                opacity: 1,
            })
            TweenMax.to(".left.navigator", 2, {
                ease: Power3.easeInOut,
                scaleX: 1
            })
            TweenMax.to(".right.navigator", 2, {
                ease: Power3.easeInOut,
                opacity: 0
            })
        // Page 1 Show
            circleMotion("first", "page-1", 2, 2, Power3.easeOut, "0vw");
            circleMotion("second", "page-1", 2, 2.3, Power3.easeOut, "15vw");
            circleMotion("third", "page-1", 2, 2.6, Power3.easeOut, "30vw");
            page_element_app(1, "h1.title", 1.5, 2);
            page_element_app(1, "h3.infoTitle", 1.5, 2);
            page_hr(1, 1.5, 2.5, "80%", 1);
    }

/*#########################################################################*/
/*                          PAGE 2B                                        */
/*#########################################################################*/
// Appear
    function page2b_app() {
        // Page 2 Diss
            circleMotion("third", "page-2", 2.5, 0, Power3.easeIn, "130vw");
            page_element_dis(2, "h3", 1.5, 0);
            page_element_dis(2, "p", 1.5, 0);
            page2_hr(1.5, 1, "0%", 0);
        // Page 3
            circleMotion("third", "page-2ex", 2.5, 1, Power3.easeOut, "0vw");
            page_element_app('2b', "h3", 1.5, 2);
            page2ex_hr('b', 1.5, 2, "90%", 1);
            TweenMax.to(".background.second", 3, {
                ease: Power3.easeOut,
                opacity: 1,
                delay: 1.5,
                scale: 1.1
            })
            TweenMax.to(".content.page-2b", 0, {
                zIndex: 5
            })
    }

// Diss
    function page2b_dis() {
        // Page 3
            circleMotion("third", "page-2ex", 2.5, 0, Power3.easeIn, "130vw");
            page_element_dis('2b', "h3", 1.5, 0);
            page2ex_hr('b', 1.5, .5, "0%", 0);
        // Page 2 Diss
            circleMotion("third", "page-2", 2.5, 1, Power3.easeOut, "0vw");
            page_element_app(2, "h3", 1.5, 2);
            page_element_app(2, "p", 1.5, 2);
            page2_hr(1.5, 2, "100%", 1);
            TweenMax.to(".background.second", 2, {
                ease: Power3.easeIn,
                opacity: 0,
                delay: .5,
                scale: 1
            })
            TweenMax.to(".content.page-2b", 0, {
                zIndex: 4,
                delay: 3
            })
    }

/*#########################################################################*/
/*                          PAGE 2C                                        */
/*#########################################################################*/
// Appear
    function page2c_app() {
        // Page 2b Diss
            circleMotion("third", "page-2ex", 2.5, 0, Power3.easeIn, "130vw");        
            page_element_dis('2b', "h3", 1.5, 0);
            page2ex_hr('b', 1.5, .5, "0%", 0);
            TweenMax.to(".content.page-2b", 0, {
                zIndex: 4,
                delay: 1.5
            })
        // Page 2c
            circleMotion("third", "page-2", 2.5, 1, Power3.easeOut, "0vw");
            TweenMax.to(".content.page-2c", 0, {
                zIndex: 5
            })
            page_element_app('2c', "h3", 1.5, 2);
            page2ex_hr('c',  1.5, 2, "90%", 1);
            TweenMax.to(".background.third", 3, {
                ease: Power3.easeOut,
                opacity: 1,
                delay: 1.5,
                scale: 1.1
            })
    }

// Diss
    function page2c_dis() {
        // Page 2c Diss
            circleMotion("third", "page-2", 2.5, 0, Power3.easeIn, "130vw");
            page_element_dis('2c', "h3", 1.5, 0);
            page2ex_hr('c',  1.5, .5, "0%", 0);
            TweenMax.to(".content.page-2c", 0, {
                zIndex: 4,
                delay: 1.5
            })
            TweenMax.to(".background.third", 3, {
                ease: Power3.easeOut,
                opacity: 0,
                delay: 1.5,
                scale: 1
            })
        // Page 2b
            circleMotion("third", "page-2ex", 2.5, 1, Power3.easeOut, "0vw");
            TweenMax.to(".content.page-2b", 0, {
                zIndex: 5
            })
            page_element_app('2b', "h3", 1.5, 2);
            page2ex_hr('b', 1.5, 2, "90%", 1);
    }

/*#########################################################################*/
/*                          PAGE 2D                                        */
/*#########################################################################*/
// Appear
    function page2d_app() {
        // Page 2c Diss
            circleMotion("third", "page-2", 2.5, 0, Power3.easeIn, "130vw");
            page_element_dis('2c', "h3", 1.5, 0);
            page2ex_hr('c',  1.5, .5, "0%", 0);
            TweenMax.to(".content.page-2c", 0, {
                zIndex: 4,
                delay: 1.5
            })
        // Page 2D
            circleMotion("third", "page-2ex", 2.5, 1, Power3.easeOut, "0vw");
            TweenMax.to(".content.page-2d", 0, {
                zIndex: 5
            })
            TweenMax.to(".right.navigator", 2, {
                ease: Power3.easeInOut,
                opacity: 0
            })
            page_element_app('2d', "h3", 1.5, 2);
            page2ex_hr('d', 1.5, 2, "90%", 1);
            TweenMax.to(".background.fourth", 3, {
                ease: Power3.easeOut,
                opacity: 1,
                delay: 1.5,
                scale: 1.1
            })
    }

// Diss
    function page2d_dis() {
        // Page 2d Diss
            circleMotion("third", "page-2ex", 2.5, 0, Power3.easeIn, "130vw");        
            page_element_dis('2d', "h3", 1.5, 0);
            page2ex_hr('d',  1.5, .5, "0%", 0);
            TweenMax.to(".content.page-2d", 0, {
                zIndex: 4,
                delay: 1.5
            })
            TweenMax.to(".background.fourth", 3, {
                ease: Power3.easeOut,
                opacity: 0,
                delay: 1.5,
                scale: 1
            })
        // Page 2c
            circleMotion("third", "page-2", 2.5, 1, Power3.easeOut, "0vw");
            TweenMax.to(".right.navigator", 1.5, {
                ease: Power3.easeInOut,
                opacity: 1
            });
            TweenMax.to(".content.page-2c", 0, {
                zIndex: 5
            })
            page_element_app('2c', "h3", 1.5, 2);
            page2ex_hr('c', 1.5, 2, "90%", 1);
    }

/*#########################################################################*/
/*                          PAGE 4                                         */
/*#########################################################################*/
// Appear
    function page4_app () {
        // Page 1 Diss
            circleMotion("first", "page-1", 2, .6, Power3.easeIn, "100vw");
            circleMotion("second", "page-1", 2, .3, Power3.easeIn, "115vw");
            circleMotion("third", "page-1", 2, 0, Power3.easeIn, "130vw");
            page_element_dis(1, "h1.title", 1.5, 0);
            page_element_dis(1, "h3.infoTitle", 1.5, 0);
            page_hr(1, 1.5, .5, "0vw", 0);
            TweenMax.to("footer .col:last-child", 1.5, {
                opacity: 0
            })
            TweenMax.to(".login button", 1, {
                opacity: 0,
                delay: 2
            })
            TweenMax.to(".content.page-4", 0, {
                zIndex: 5,
                delay: 2
            })
        // Page 4 App
            TweenMax.to(".circle.third.page-4", 2, {
                top: "242%",
                x: "30vw",
                Y: "-50%",
                delay: 1.5,
                scale: 1
            })
            circleMotion("first", "page-1", 2.2, 2.7, Power3.easeOut, "0vw");
            circleMotion("second", "page-1", 2.2, 3, Power3.easeOut, "15vw");
            page_element_app(4, "*", 2, 2);
            TweenMax.to("#wrapper", 0, {
                zIndex: 5,
                delay: 2
            })
    }

// Diss
    function page4_dis () {
        // Page 4 Diss
            TweenMax.to(".circle.third.page-4", 2, {
                top: "0",
                x: "34.6vw",
                Y: "-43.8%",
                scale: 0,
                delay: 1
            })
            circleMotion("first", "page-1", 2, .6, Power3.easeIn, "100vw");
            circleMotion("second", "page-1", 2, .3, Power3.easeIn, "115vw");
            circleMotion("third", "page-1", 2, 0, Power3.easeIn, "130vw");
            page4_element_dis("*", 2, 0);
            TweenMax.to("#wrapper", 0, {
                zIndex: 2,
                delay: 3
            })
            TweenMax.to(".content.page-4", 0, {
                zIndex: 2,
                delay: 4
            })
        // Page 1 App
            circleMotion("first", "page-1", 2, 3, Power3.easeOut, "0vw");
            circleMotion("second", "page-1", 2, 3.3, Power3.easeOut, "15vw");
            circleMotion("third", "page-1", 2, 3.6, Power3.easeOut, "30vw");
            page_element_app(1, "h1.title", 1.5, 3);
            page_element_app(1, "h3.infoTitle", 1.5, 3);
            page_hr(1, 1.5, 3.5, "80%", 1);
            TweenMax.to("footer .col:last-child", 1.5, {
                opacity: 1,
                delay: 3
            })
            TweenMax.to(".login button", 1, {
                opacity: 1,
                delay: 3
            })
    }

/*#########################################################################*/
/*                          PAGE 5                                         */
/*#########################################################################*/
// Appear
    function page5_app(type) {
        // Page 4 Diss
            TweenMax.to(".circle.third.page-4", 2, {
                x: "100vw",
                ease: Power3.easeIn,
            })
            circleMotion("first", "page-1", 2, .45, Power3.easeIn, "100vw");
            circleMotion("second", "page-1", 2, .15, Power3.easeIn, "115vw");
            circleMotion("third", "page-1", 2, 0, Power3.easeIn, "130vw");
            TweenMax.to(".content.page-4 .row", 2, {
                x: "100vw",
                ease: Power3.easeIn,
                delay: .15
            })
            TweenMax.to("#wrapper", 0, {
                zIndex: 2,
                delay: 3
            })
            TweenMax.to(".content.page-4", 0, {
                zIndex: 2,
                delay: 4
            })
        // Page 5 App
            circleMotion("first", "page-5", 2, 1.2, Power3.easeOut, "0vw");
            circleMotion("second", "page-5", 2, 1.5, Power3.easeOut, "15vw");
            circleMotion("third", "page-5", 2, 1.8, Power3.easeOut, "30vw");
            TweenMax.to(".content.page-5." + type, 2, {
                x: "0vw",
                ease: Power3.easeOut,
                delay: 1.8
            })
            TweenMax.to(".content.page-5." + type, 0, {
                zIndex: 5,
            })
            TweenMax.to(".background.first", 3, {
                ease: Power3.easeInOut,
                scale: 1.2,
                delay: 1
            })
    }

// Diss
    function page5_dis (type) {
        // Page 5 Dis
            TweenMax.to(".background.first", 3, {
                ease: Power3.easeInOut,
                scale: 1,
                delay: 1
            })
            circleMotion("first", "page-5", 2, .6, Power3.easeIn, "100vw");
            circleMotion("second", "page-5", 2, .3, Power3.easeIn, "115vw");
            circleMotion("third", "page-5", 2, 0, Power3.easeIn, "130vw");
            TweenMax.to(".content.page-5." + type, 2, {
                x: "100vw",
                ease: Power3.easeIn,
            })
            TweenMax.to(".content.page-5." + type, 0, {
                zIndex: 4,
                delay: 3
            })
        // Page 4 App
            TweenMax.to(".circle.third.page-4", 2, {
                x: "30vw",
                delay: 2.4,
            })
            TweenMax.to(".content.page-4 .row", 2, {
                x: "0vw",
                ease: Power3.easeOut,
                delay: 2.7
            })
            circleMotion("first", "page-1", 2.2, 2, Power3.easeOut, "0vw");
            circleMotion("second", "page-1", 2.2, 2.4, Power3.easeOut, "15vw");
            TweenMax.to("#wrapper", 0, {
                zIndex: 5,
                delay: 2
            })
            TweenMax.to(".content.page-4", 0, {
                zIndex: 5,
                delay: 2.2
            })
    }